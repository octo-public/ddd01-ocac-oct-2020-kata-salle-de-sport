import { expect }             from 'chai'
import { AbonnementSouscrit } from '../domaine/abonnement/abonnement-souscrit'
import { MockEmailSender }    from '../infrastructure/mock-email-sender'
import { AbonnementSouscritListener } from './abonnement-souscrit-listener'

describe('Abonnement souscrit listener', () => {
	it('Envoyer un email quand l\'abonnent est souscrit', () => {
		// GIVEN
		const emailSender = new MockEmailSender()
		const event = new AbonnementSouscrit(1, 30, "email@gmail.com")

		// WHEN
		new AbonnementSouscritListener(emailSender).listen(event)

		// THEN
		expect(emailSender.isCalledWith).to.eql("email@gmail.com")
	})
})
