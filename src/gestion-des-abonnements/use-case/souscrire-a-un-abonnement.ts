import { CommandHandler }       from '../../magic-bus/command/CommandHandler'
import { Abonnement}            from '../domaine/abonnement/abonnement'
import { AbonnementEmail }      from '../domaine/abonnement/abonnement-email'
import { AbonnementRepository } from '../domaine/abonnement/abonnement-repository'
import { EventPublisher }       from '../infrastructure/event-publisher'
import { SouscrireAbonnement }  from './souscrire-abonnement'

export class SouscrireAbonnementUseCase implements CommandHandler {
	constructor(private abonnementRepository: AbonnementRepository,
							private eventPublisher: EventPublisher) {
	}

	execute(command: SouscrireAbonnement) {
		const abonnement = Abonnement.souscrire(command.duree, command.estEtudiant, new AbonnementEmail(command.email))

		this.abonnementRepository.sauvegarder(abonnement)
		this.eventPublisher.publish(abonnement.getEvents())
	}
}
