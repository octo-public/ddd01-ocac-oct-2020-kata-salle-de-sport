import assert                         from 'assert'
import { expect }                   from 'chai'
import { Abonnement }               from '../domaine/abonnement/abonnement'
import { AbonnementEmail }          from '../domaine/abonnement/abonnement-email'
import { AbonnementSouscrit }       from '../domaine/abonnement/abonnement-souscrit'
import { FormatDeMailNonValide }    from '../domaine/abonnement/format-de-mail-non-valide'
import { MockAbonnementRepository } from '../infrastructure/mock-abonnement-repository'
import { MockEventPublisher }         from '../infrastructure/mock-event-publisher'
import { SouscrireAbonnementUseCase } from './souscrire-a-un-abonnement'
import { SouscrireAbonnement }        from './souscrire-abonnement'

describe('Souscrire à un abonnement', () => {
	it('L\'abonnement annuel pour un non étudiant a été souscrit', async () => {
		// GIVEN
		const eventPublisher       = new MockEventPublisher()
		const abonnementRepository = new MockAbonnementRepository()
		const command              = new SouscrireAbonnement(12, false, "monemail@gmail.com")

		// WHEN
		await new SouscrireAbonnementUseCase(abonnementRepository, eventPublisher).execute(command)

		// THEN
		expect(eventPublisher.isCalledWith).to.eql([new AbonnementSouscrit(12, 100, "monemail@gmail.com")])
		expect(abonnementRepository.get()).to.eql(Abonnement.souscrire(12, false, new AbonnementEmail("monemail@gmail.com")))
	})

	it('L\'abonnement annuel pour un étudiant a été souscrit avec une réduction', async () => {
		// GIVEN
		const eventPublisher       = new MockEventPublisher()
		const abonnementRepository = new MockAbonnementRepository()
		const command              = new SouscrireAbonnement(12, true, 'monemail@gmail.com')

		// WHEN
		await new SouscrireAbonnementUseCase(abonnementRepository, eventPublisher).execute(command)

		// THEN
		expect(eventPublisher.isCalledWith).to.eql([new AbonnementSouscrit(12, 80, 'monemail@gmail.com')])
		expect(abonnementRepository.get()).to.eql(Abonnement.souscrire(12, true, new AbonnementEmail('monemail@gmail.com')))
	})

	it('Informer si le mail ne se terminer pas .com', async () => {
		// GIVEN
		const eventPublisher       = new MockEventPublisher()
		const abonnementRepository = new MockAbonnementRepository()
		const command              = new SouscrireAbonnement(12, true, 'monemail@gmail.fr')

		try {
			// WHEN
			await new SouscrireAbonnementUseCase(abonnementRepository, eventPublisher).execute(command)
			assert.fail()
		} catch (e) {
			// THEN
			expect(e).to.instanceOf(FormatDeMailNonValide)
		}
	})
})
