import { EventListener }      from '../../magic-bus/event/EventListener'
import { AbonnementSouscrit } from '../domaine/abonnement/abonnement-souscrit'
import { EmailSender }        from '../domaine/email-sender'

export class AbonnementSouscritListener implements EventListener {
	constructor(private emailSender: EmailSender) {
	}

	listen(event: AbonnementSouscrit) {
		this.emailSender.send(event.email)
	}
}
