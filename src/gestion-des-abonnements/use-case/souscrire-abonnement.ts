import { Command } from '../../magic-bus/command/Command'
import { Event }   from '../../magic-bus/event/Event'

export class SouscrireAbonnement implements Command {
	readonly name = 'SouscrireAbonnement'

	constructor(readonly duree: number, readonly estEtudiant: boolean, readonly email: string) {
	}
}
