import { EmailSender } from '../domaine/email-sender'

export class MockEmailSender implements EmailSender {
	isCalledWith: any = null

	send(email: string) {
		this.isCalledWith = email
	}
}
