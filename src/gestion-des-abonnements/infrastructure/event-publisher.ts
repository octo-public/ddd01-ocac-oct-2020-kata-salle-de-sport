export interface EventPublisher {
	publish(events: any): void
}
