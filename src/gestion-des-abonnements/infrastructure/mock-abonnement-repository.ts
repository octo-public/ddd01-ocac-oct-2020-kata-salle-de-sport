import { Abonnement }           from '../domaine/abonnement/abonnement'
import { AbonnementRepository } from '../domaine/abonnement/abonnement-repository'

export class MockAbonnementRepository implements AbonnementRepository {
	private abonnement: Abonnement | null = null

	get(): Abonnement | null {
		return this.abonnement
	}

	sauvegarder(abonnement: Abonnement) {
		this.abonnement = abonnement
	}
}
