import { EventPublisher } from './event-publisher'

export class MockEventPublisher implements EventPublisher {
	isCalledWith: any

	publish(events: any) {
		this.isCalledWith = events
	}
}
