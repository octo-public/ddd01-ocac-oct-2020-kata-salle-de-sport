export interface EmailSender {
	send(email: string): void
}
