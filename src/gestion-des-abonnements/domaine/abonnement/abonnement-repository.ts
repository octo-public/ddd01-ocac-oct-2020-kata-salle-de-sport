import { Abonnement } from './abonnement'

export interface AbonnementRepository {
	get(): Abonnement | null

	sauvegarder(abonnement: Abonnement): void
}
