import { AbonnementEmail }    from './abonnement-email'
import { AbonnementSouscrit } from './abonnement-souscrit'

export class Abonnement {
	private events: any[] = []

	private prixDeBase = 100

	constructor(private readonly duree: number, private readonly isEtudiant: boolean) {
	}

	static souscrire(duree: number, estEtudiant: boolean, email: AbonnementEmail): Abonnement {
		const abonnement = new Abonnement(duree, estEtudiant)
		abonnement.events.push(new AbonnementSouscrit(
			duree,
			abonnement.calculePrixAvecReduction(estEtudiant, abonnement.prixDeBase),
			email.email
		))
		return abonnement
	}

	getEvents() {
		return this.events
	}

	private calculePrixAvecReduction(isEtudiant: boolean, prixDeBase: number) {
		if (isEtudiant) {
			return prixDeBase * .8
		} else {
			return prixDeBase
		}
	}
}
