import { FormatDeMailNonValide } from './format-de-mail-non-valide'

export class AbonnementEmail {
	constructor(readonly email: string) {
		if (!email.endsWith('.com')) {
			throw new FormatDeMailNonValide()
		}
	}
}
