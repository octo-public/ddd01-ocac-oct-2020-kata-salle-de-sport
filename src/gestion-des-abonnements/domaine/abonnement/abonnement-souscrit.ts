import { Event } from '../../../magic-bus/event/Event'

export class AbonnementSouscrit implements Event {
	readonly createdAt: Date = null
	readonly name = 'AbonnementSouscrit'

	constructor(readonly duree: number, readonly prix: number, readonly email: string) {
	}
}
