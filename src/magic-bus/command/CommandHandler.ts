import { Command } from './Command'

export interface CommandHandler {
    execute(command: Command): any
}
