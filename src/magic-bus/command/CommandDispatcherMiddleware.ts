import { Command } from './Command'

export interface CommandDispatcherMiddleware {
    handle(command: Command): void
}
