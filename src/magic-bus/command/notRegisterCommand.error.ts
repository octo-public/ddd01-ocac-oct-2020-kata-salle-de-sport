export class NotRegisterCommandError extends Error {
    constructor() {
        super('The dispatched command is not registered')
    }
}
