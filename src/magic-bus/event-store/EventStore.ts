import { Event } from '../event/Event'

export interface EventStore {
    fetch(): Event[]

    store(event: Event): void
}
