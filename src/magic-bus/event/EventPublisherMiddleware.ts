import { Event } from './Event'

export interface EventPublisherMiddleware {
    handle(event: Event): void
}
