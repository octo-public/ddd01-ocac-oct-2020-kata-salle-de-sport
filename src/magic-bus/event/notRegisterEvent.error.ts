export class NotRegisterEventError extends Error {
    constructor() {
        super('The published event is not registered')
    }
}
