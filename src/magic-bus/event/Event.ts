export interface Event {
    readonly name: string
    readonly createdAt: Date
}
