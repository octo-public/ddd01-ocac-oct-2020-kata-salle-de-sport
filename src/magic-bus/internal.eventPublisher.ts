import { EventStore }               from './event-store/EventStore'
import { Event }                    from './event/Event'
import { EventListener }            from './event/EventListener'
import { EventPublisher }           from './event/EventPublisher'
import { EventPublisherMiddleware } from './event/EventPublisherMiddleware'
import { NotRegisterEventError }    from './event/notRegisterEvent.error'

export class InternalEventPublisher implements EventPublisher {
	private listeners: { [event: string]: EventListener[] } = {}
	private middleware: EventPublisherMiddleware[]          = []

	constructor(private eventStore: EventStore = null) {
	}

	registerListeners(eventListeners: { [event: string]: EventListener[] }): EventPublisher {
		this.listeners = eventListeners
		return this
	}

	registerMiddleware(eventPublisherMiddleware: EventPublisherMiddleware[]): EventPublisher {
		this.middleware = eventPublisherMiddleware
		return this
	}

	async publish(event: Event[]): Promise<void> {
		event.forEach(e => {
			this.middleware.forEach(m => m.handle(e))
			this.listeners[e.name].forEach(l => l.listen(e))
		})
	}
}
