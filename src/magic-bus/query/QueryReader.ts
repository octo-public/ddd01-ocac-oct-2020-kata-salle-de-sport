import { Query }                 from './Query'
import { QueryHandler }          from './QueryHandler'
import { QueryReaderMiddleware } from './QueryReaderMiddleware'

export interface QueryReader {
    registerHandlers(queryHandlers: { [query: string]: QueryHandler }): QueryReader

    registerMiddleware(queryReaderMiddleware: QueryReaderMiddleware[]): QueryReader

    dispatch<R>(query: Query): Promise<R>
}
