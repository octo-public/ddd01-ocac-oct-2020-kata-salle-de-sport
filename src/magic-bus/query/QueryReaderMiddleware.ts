import { Query } from './Query'

export interface QueryReaderMiddleware {
    handle(query: Query): void
}
