import { Abonnement }                 from './gestion-des-abonnements/domaine/abonnement/abonnement'
import { AbonnementRepository }       from './gestion-des-abonnements/domaine/abonnement/abonnement-repository'
import { AbonnementSouscrit }         from './gestion-des-abonnements/domaine/abonnement/abonnement-souscrit'
import { EmailSender }                from './gestion-des-abonnements/domaine/email-sender'
import { AbonnementSouscritListener } from './gestion-des-abonnements/use-case/abonnement-souscrit-listener'
import { SouscrireAbonnementUseCase } from './gestion-des-abonnements/use-case/souscrire-a-un-abonnement'
import { SouscrireAbonnement }        from './gestion-des-abonnements/use-case/souscrire-abonnement'
import { InternalEventPublisher }     from './magic-bus/internal.eventPublisher'

let abonnement: Abonnement = null
const abonnementRepository: AbonnementRepository = {
	get(): Abonnement | null {
		return abonnement
	},

	sauvegarder(abonnement: Abonnement): void {
		console.log('j\'ai sauvegardé mon abonnement')
		abonnement = abonnement
	}
}

const emailSender: EmailSender = {
	send(email: string): void {
		console.log(`Un mail a été envoyé à : ${ email }`)
	}
}



const eventPublisher = new InternalEventPublisher()
	.registerListeners({
		[AbonnementSouscrit.name]: [new AbonnementSouscritListener(emailSender)]
	})

const command = new SouscrireAbonnement(12, true, "etudiant@gmail.com")

new SouscrireAbonnementUseCase(abonnementRepository, eventPublisher)
	.execute(command)
