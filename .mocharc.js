module.exports = {
    diff: true,
    extension: ['ts'],
    opts: false,
    package: './package.json',
    reporter: 'spec',
    slow: 75,
    timeout: 1000,
    sort: true,
    require: ['ts-node/register'],
    ui: 'bdd',
    spec: ['src/**/*.spec.ts'],
    exit: true
}
